package optimize;

import submit.MySolver;
import joeq.Class.jq_Class;
import joeq.Compiler.Quad.ControlFlowGraph;
import joeq.Compiler.Quad.Operand;
import joeq.Compiler.Quad.Operator;
import joeq.Compiler.Quad.Quad;
import joeq.Compiler.Quad.QuadIterator;
import joeq.Compiler.Quad.QuadVisitor;
import joeq.Compiler.Quad.Operand.RegisterOperand;
import joeq.Main.Helper;

import java.util.Set;
import java.util.TreeSet;

import flow.Flow;

public class FindRedundantNullChecks {
    
    static class NullAnalysis extends SimpleOptimizingAnalysis {
        
        static class NullCheckTable implements Flow.DataflowObject {
            private Set<String> unchecked;
            private Set<String> defined;
            
            public NullCheckTable() {
                unchecked = new TreeSet<String>();
                defined = new TreeSet<String>();
            }
            
            public void setToTop() {
                unchecked.clear();
                defined.clear();
            }
            
            public void setToBottom() {
                System.err.println("====Trying to Set to Bottom====");
                unchecked.clear();
                defined.clear();
            }
            
            public void meetWith(Flow.DataflowObject o) {
                NullCheckTable a = (NullCheckTable) o;
                unchecked.addAll(a.unchecked);
                defined.addAll(a.defined);
            }
            
            public void copy(Flow.DataflowObject o) {
                NullCheckTable a = (NullCheckTable) o;
                unchecked.clear();
                unchecked.addAll(a.unchecked);
                defined.clear();
                defined.addAll(a.defined);
            }
            
            @Override
            public String toString() {
                return unchecked.toString();
            }
            
            @Override
            public boolean equals(Object o) {
                if (!(o instanceof NullCheckTable)) {
                    return false;
                }
                NullCheckTable a = (NullCheckTable) o;
                return unchecked.equals(a.unchecked) && defined.equals(a.defined);
            }
            
            @Override
            public int hashCode() {
                return unchecked.hashCode();
            }
            
            public boolean contains(String register) {
                return unchecked.contains(register);
            }
            
            public boolean setChecked(String register) {
                return unchecked.remove(register);
            }
            public void clearChecked() {
                unchecked.addAll(defined);
            }
            
            public void setDefined(String register) {
                unchecked.add(register);
                defined.add(register);
            }
        }
        
        private NullCheckTable[] in, out;
        private NullCheckTable entry, exit;
        private boolean printRedundant = false;
        
        public NullAnalysis() {
        }
        
        public NullAnalysis(boolean printRedundant) {
            this.printRedundant = printRedundant;
        }
        
        public void preprocess(ControlFlowGraph cfg) {
            /* Generate initial conditions. */
            QuadIterator qit = new QuadIterator(cfg);
            int max = 0;
            while (qit.hasNext()) {
                int x = qit.next().getID();
                if (x > max)
                    max = x;
            }
            max += 1;
            in = new NullCheckTable[max];
            out = new NullCheckTable[max];
            
            entry = new NullCheckTable();
            exit = new NullCheckTable();
            transferfn.val = new NullCheckTable();
            for (int i = 0; i < in.length; i++) {
                in[i] = new NullCheckTable();
                out[i] = new NullCheckTable();
            }
            
            int numargs = cfg.getMethod().getParamTypes().length;
            for (int i = 0; i < numargs; i++) {
                entry.setDefined("R" + i);
            }
        }
        
        protected boolean isRedundant(Quad quad) {
            if (quad.getOperator() instanceof Operator.NullCheck) {
                Operand op = Operator.NullCheck.getSrc(quad);
                if (op instanceof RegisterOperand) {
                    String register = ((RegisterOperand) op).getRegister().toString();
                    if(!in[quad.getID()].contains(register)) {
                        return true;
                    }
                }
            }
            return false;
        }
        
        public void postprocess(ControlFlowGraph cfg) {
            if (printRedundant) {
                System.out.printf("%s", cfg.getMethod().getName().toString());
                TreeSet<Integer> redundantChecks = listRedundant(cfg);
                for (Integer id : redundantChecks) {
                    System.out.printf(" %d", id);
                }
                System.out.println();
            }
        }
        
        /* Is this a forward dataflow analysis? */
        public boolean isForward() {
            return true;
        }
        
        /* Routines for interacting with dataflow values. */
        
        public Flow.DataflowObject getEntry() {
            Flow.DataflowObject result = newTempVar();
            result.copy(entry);
            return result;
        }
        
        public Flow.DataflowObject getExit() {
            Flow.DataflowObject result = newTempVar();
            result.copy(exit);
            return result;
        }
        
        public Flow.DataflowObject getIn(Quad q) {
            Flow.DataflowObject result = newTempVar();
            result.copy(in[q.getID()]);
            return result;
        }
        
        public Flow.DataflowObject getOut(Quad q) {
            Flow.DataflowObject result = newTempVar();
            result.copy(out[q.getID()]);
            return result;
        }
        
        public void setIn(Quad q, Flow.DataflowObject value) {
            in[q.getID()].copy(value);
        }
        
        public void setOut(Quad q, Flow.DataflowObject value) {
            out[q.getID()].copy(value);
        }
        
        public void setEntry(Flow.DataflowObject value) {
            entry.copy(value);
        }
        
        public void setExit(Flow.DataflowObject value) {
            exit.copy(value);
        }
        
        public Flow.DataflowObject newTempVar() {
            return new NullCheckTable();
        }
        
        /*
         * Actually perform the transfer operation on the relevant quad.
         */
        
        private TransferFunction transferfn = new TransferFunction();
        
        public void processQuad(Quad q) {
            transferfn.val.copy(in[q.getID()]);
            Helper.runPass(q, transferfn);
            out[q.getID()].copy(transferfn.val);
        }
        
        /* The QuadVisitor that actually does the computation */
        public class TransferFunction extends QuadVisitor.EmptyVisitor {
            NullCheckTable val;
            
            @Override
            public void visitQuad(Quad q) {
                for (RegisterOperand def : q.getDefinedRegisters()) {
                    val.setDefined(def.getRegister().toString());
                }
            }
            
            /*
             * Be safe: assume method invocations and special quads can have
             * arbitrary side effects on registers, invalidating previous analysis.
             */
            @Override
            public void visitInvoke(Quad q) {
                val.clearChecked();
            }
            @Override
            public void visitSpecial(Quad q) {
                val.clearChecked();
            }
            
            @Override
            public void visitNewArray(Quad q) {
                Operand op = Operator.NewArray.getDest(q);
                String register = null;
                if (op instanceof RegisterOperand) {
                    register = ((RegisterOperand) op).getRegister().toString();
                    val.setChecked(register);
                }
            }
            
            @Override
            public void visitNullCheck(Quad q) {
                Operand op = Operator.NullCheck.getSrc(q);
                String register = null;
                if (op instanceof RegisterOperand) {
                    register = ((RegisterOperand) op).getRegister().toString();
                    val.setChecked(register);
                }
            }
        }
    }
    
    private static void runSolver(jq_Class c) {
        MySolver solver = new MySolver();
        NullAnalysis analysis = new NullAnalysis(true);
        solver.registerAnalysis(analysis);
        Helper.runPass(c, solver);
    }
    
    /*
     * args is an array of class names method should print out a list of quad
     * ids of redundant null checks for each function as described on the course
     * webpage
     */
    public static void main(String[] args) {
        jq_Class[] classes = new jq_Class[args.length];
        for (int i = 0; i < classes.length; i++) {
            classes[i] = (jq_Class) Helper.load(args[i]);
        }
        
        // visit each of the specified classes with the solver.
        for (int i = 0; i < classes.length; i++) {
            runSolver(classes[i]);
        }
    }
}
