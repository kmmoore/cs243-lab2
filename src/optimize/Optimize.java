package optimize;

import java.util.List;

import flow.Liveness;
import joeq.Class.jq_Class;
import joeq.Main.Helper;
import submit.MySolver;
import optimize.FindRedundantNullChecks;
import optimize.FindRedundantBoundsChecks;

public class Optimize {
    /*
     * optimizeFiles is a list of names of class that should be optimized
     * if nullCheckOnly is true, disable all optimizations except "remove redundant NULL_CHECKs."
     */
    public static void optimize(List<String> optimizeFiles, boolean nullCheckOnly) {
        for (int i = 0; i < optimizeFiles.size(); i++) {
            jq_Class classes = (jq_Class)Helper.load(optimizeFiles.get(i));

            MySolver solver = new MySolver();
            OptimizingAnalysis analysis = new FindRedundantNullChecks.NullAnalysis();
            solver.registerOptimizingAnalysis(analysis);
            Helper.runPass(classes, solver);
            
            if(!nullCheckOnly) {
                OptimizingAnalysis[] additionalAnalyses = new OptimizingAnalysis[] {
                    new Liveness(),
                    new FindRedundantBoundsChecks.BoundsAnalysis(),
                };

                for(OptimizingAnalysis optimizingAnalysis: additionalAnalyses) {
                    solver.registerOptimizingAnalysis(optimizingAnalysis);
                    Helper.runPass(classes, solver);
                }
            }
        }
    }
}
