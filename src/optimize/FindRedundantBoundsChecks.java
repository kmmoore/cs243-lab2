package optimize;

import submit.MySolver;
import joeq.Class.jq_Class;
import joeq.Compiler.Quad.ControlFlowGraph;
import joeq.Compiler.Quad.Operand;
import joeq.Compiler.Quad.Operator;
import joeq.Compiler.Quad.Quad;
import joeq.Compiler.Quad.QuadIterator;
import joeq.Compiler.Quad.QuadVisitor;
import joeq.Compiler.Quad.Operand.RegisterOperand;
import joeq.Main.Helper;

import java.util.Set;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.Iterator;

import flow.Flow;

public class FindRedundantBoundsChecks {
    
    static class BoundsAnalysis extends SimpleOptimizingAnalysis {

        static class BoundsCheck {
            public final String arrayName;
            public final String indexName;

            public BoundsCheck(String aN, String iN) {
                arrayName = aN;
                indexName = iN;
            }

            @Override
            public boolean equals (Object o) {
                if (o == null || !(o instanceof BoundsCheck)) return false;
                BoundsCheck a = (BoundsCheck)o;

                return arrayName.equals(a.arrayName) && indexName.equals(a.indexName);
            }

            @Override
            public int hashCode() {
                return 31 * arrayName.hashCode() + indexName.hashCode();
            }

            @Override
            public String toString() {
                return arrayName + "[" + indexName + "]";
            }
        }
        
        static class BoundsCheckTable implements Flow.DataflowObject {
            private Set<BoundsCheck> checked;
            private boolean isTop;
            
            public BoundsCheckTable() {
                checked = new HashSet<BoundsCheck>();
                isTop = true;
            }
            
            public void setToTop() {
                checked.clear();
                isTop = true;
            }
            
            public void setToBottom() {
                System.err.println("====Trying to Set to Bottom====");
                checked.clear();
            }
            
            public void meetWith(Flow.DataflowObject o) {
                BoundsCheckTable a = (BoundsCheckTable) o;
                if (a.isTop) return;

                if (isTop) {
                    isTop = false;
                    checked.addAll(a.checked);
                } else {
                    checked.retainAll(a.checked);
                }
            }
            
            public void copy(Flow.DataflowObject o) {
                BoundsCheckTable a = (BoundsCheckTable) o;
                checked.clear();
                checked.addAll(a.checked);
                isTop = a.isTop;
            }
            
            @Override
            public String toString() {
                if (isTop) return "TOP";
                return checked.toString();
            }
            
            @Override
            public boolean equals(Object o) {
                if (!(o instanceof BoundsCheckTable)) {
                    return false;
                }
                BoundsCheckTable a = (BoundsCheckTable) o;

                return isTop == a.isTop && checked.equals(a.checked);
            }
            
            @Override
            public int hashCode() {
                return checked.hashCode();
            }
            
            public boolean contains(String array, String index) {
                return checked.contains(new BoundsCheck(array, index));
            }
            
            public boolean setChecked(String array, String index) {
                isTop = false;
                return checked.add(new BoundsCheck(array, index));
            }
            
            public boolean setDefined(String register) {
                Iterator<BoundsCheck> it = checked.iterator();
                while (it.hasNext()) {
                    BoundsCheck b = it.next();
                    if (b.arrayName.equals(register) || b.indexName.equals(register)) {
                        isTop = false;
                        it.remove();
                    }
                }
                return true;
            }
        }
        
        private BoundsCheckTable[] in, out;
        private BoundsCheckTable entry, exit;
        private boolean printRedundant = false;
        
        public BoundsAnalysis() {
        }
        
        public BoundsAnalysis(boolean printRedundant) {
            this.printRedundant = printRedundant;
        }
        
        public void preprocess(ControlFlowGraph cfg) {
            /* Generate initial conditions. */
            QuadIterator qit = new QuadIterator(cfg);
            int max = 0;
            while (qit.hasNext()) {
                int x = qit.next().getID();
                if (x > max)
                    max = x;
            }
            max += 1;
            in = new BoundsCheckTable[max];
            out = new BoundsCheckTable[max];
            
            entry = new BoundsCheckTable();
            exit = new BoundsCheckTable();
            transferfn.val = new BoundsCheckTable();
            for (int i = 0; i < in.length; i++) {
                in[i] = new BoundsCheckTable();
                out[i] = new BoundsCheckTable();
            }
            
            // int numargs = cfg.getMethod().getParamTypes().length;
            // for (int i = 0; i < numargs; i++) {
            //     entry.setDefined("R" + i);
            // }
        }
        
        protected boolean isRedundant(Quad quad) {
            if (quad.getOperator() instanceof Operator.BoundsCheck) {
                Operand ref = Operator.BoundsCheck.getRef(quad);
                Operand index = Operator.BoundsCheck.getIndex(quad);
                if (ref instanceof RegisterOperand && index instanceof RegisterOperand) {
                    String refStr = ((RegisterOperand) ref).getRegister().toString();
                    String indexStr = ((RegisterOperand) index).getRegister().toString();

                    if(in[quad.getID()].contains(refStr, indexStr)) {
                        return true;
                    }
                }
            }
            return false;
        }
        
        public void postprocess(ControlFlowGraph cfg) {
            if (printRedundant) {
                System.out.printf("%s", cfg.getMethod().getName().toString());

                // System.out.println("");
                // System.out.println("entry: " + entry.toString());
                // for (int i = 1; i < in.length; i++) {
                //     System.out.println(i + " in:  " + in[i].toString());
                //     System.out.println(i + " out: " + out[i].toString());
                // }
                // System.out.println("exit: " + exit.toString());
                
                TreeSet<Integer> redundantChecks = listRedundant(cfg);
                for (Integer id : redundantChecks) {
                    System.out.printf(" %d", id);
                }
                System.out.println();
            }
        }
        
        /* Is this a forward dataflow analysis? */
        public boolean isForward() {
            return true;
        }
        
        /* Routines for interacting with dataflow values. */
        
        public Flow.DataflowObject getEntry() {
            Flow.DataflowObject result = newTempVar();
            result.copy(entry);
            return result;
        }
        
        public Flow.DataflowObject getExit() {
            Flow.DataflowObject result = newTempVar();
            result.copy(exit);
            return result;
        }
        
        public Flow.DataflowObject getIn(Quad q) {
            Flow.DataflowObject result = newTempVar();
            result.copy(in[q.getID()]);
            return result;
        }
        
        public Flow.DataflowObject getOut(Quad q) {
            Flow.DataflowObject result = newTempVar();
            result.copy(out[q.getID()]);
            return result;
        }
        
        public void setIn(Quad q, Flow.DataflowObject value) {
            in[q.getID()].copy(value);
        }
        
        public void setOut(Quad q, Flow.DataflowObject value) {
            out[q.getID()].copy(value);
        }
        
        public void setEntry(Flow.DataflowObject value) {
            entry.copy(value);
        }
        
        public void setExit(Flow.DataflowObject value) {
            exit.copy(value);
        }
        
        public Flow.DataflowObject newTempVar() {
            return new BoundsCheckTable();
        }
        
        /*
         * Actually perform the transfer operation on the relevant quad.
         */
        
        private TransferFunction transferfn = new TransferFunction();
        
        public void processQuad(Quad q) {
            transferfn.val.copy(in[q.getID()]);
            Helper.runPass(q, transferfn);
            out[q.getID()].copy(transferfn.val);
        }
        
        /* The QuadVisitor that actually does the computation */
        public class TransferFunction extends QuadVisitor.EmptyVisitor {
            BoundsCheckTable val;
            
            @Override
            public void visitQuad(Quad q) {
                for (RegisterOperand def : q.getDefinedRegisters()) {
                    val.setDefined(def.getRegister().toString());
                }
            }
            
            @Override
            public void visitBoundsCheck(Quad q) {
                Operand ref = Operator.BoundsCheck.getRef(q);
                Operand index = Operator.BoundsCheck.getIndex(q);
                if (ref instanceof RegisterOperand && index instanceof RegisterOperand) {
                    String refStr = ((RegisterOperand) ref).getRegister().toString();
                    String indexStr = ((RegisterOperand) index).getRegister().toString();
                    val.setChecked(refStr, indexStr);
                }
            }
        }
    }
    
    private static void runSolver(jq_Class c) {
        MySolver solver = new MySolver();
        BoundsAnalysis analysis = new BoundsAnalysis(true);
        solver.registerAnalysis(analysis);
        Helper.runPass(c, solver);
    }
    
    /*
     * args is an array of class names method should print out a list of quad
     * ids of redundant null checks for each function as described on the course
     * webpage
     */
    public static void main(String[] args) {
        jq_Class[] classes = new jq_Class[args.length];
        for (int i = 0; i < classes.length; i++) {
            classes[i] = (jq_Class) Helper.load(args[i]);
        }
        
        // visit each of the specified classes with the solver.
        for (int i = 0; i < classes.length; i++) {
            runSolver(classes[i]);
        }
    }
}
