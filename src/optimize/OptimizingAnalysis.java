package optimize;

import joeq.Compiler.Quad.ControlFlowGraph;
import flow.Flow.Analysis;

//import java.util.TreeSet;

public interface OptimizingAnalysis extends Analysis {
    //Returns true if any quads were removed
    //When repeatedly called, must eventually converge after a finite number of iterations
    //(This is guaranteed if it only removes quads from cfg)
    public boolean doOptimize(ControlFlowGraph cfg);
}
