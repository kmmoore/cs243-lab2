package optimize;

import java.util.Set;
import java.util.TreeSet;

import joeq.Compiler.Quad.ControlFlowGraph;
import joeq.Compiler.Quad.Quad;
import joeq.Compiler.Quad.QuadIterator;

public abstract class SimpleOptimizingAnalysis implements OptimizingAnalysis {
    protected abstract boolean isRedundant(Quad quad);
    
    /*
     * Add IDs of redundant quads to redundantSet if it's not null
     * otherwise just remove them.
     */
    private boolean iterateCfg(ControlFlowGraph cfg, Set<Integer> redundantSet) {
        boolean changed = false;
        QuadIterator quadIter = new QuadIterator(cfg);
        while (quadIter.hasNext()) {
            Quad quad = quadIter.next();
            if(isRedundant(quad)) {
                if(redundantSet == null) {
                    quadIter.remove();
                } else {
                    redundantSet.add(quad.getID());
                }
                changed = true;
            }
        }
        return changed;
    }
    public TreeSet<Integer> listRedundant(ControlFlowGraph cfg) {
        TreeSet<Integer> redundantSet = new TreeSet<Integer>();
        iterateCfg(cfg, redundantSet);
        return redundantSet;
    }
    public boolean doOptimize(ControlFlowGraph cfg) {
        return iterateCfg(cfg, null);
    }
}
