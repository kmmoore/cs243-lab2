package submit;

// some useful things to import. add any additional imports you need.
import java.util.*;

import optimize.OptimizingAnalysis;
import joeq.Compiler.Quad.*;
import flow.Flow;

/**
 * Skeleton class for implementing the Flow.Solver interface.
 */
public class MySolver implements Flow.Solver {

    protected Flow.Analysis analysis;
    private boolean optimizing = false;
    

    /**
     * Sets the analysis. When visitCFG is called, it will perform this analysis
     * on a given CFG.
     * 
     * @param analyzer
     *            The analysis to run
     */
    public void registerAnalysis(Flow.Analysis analyzer) {
        this.analysis = analyzer;
    }
    
    public void registerOptimizingAnalysis(OptimizingAnalysis analyzer) {
        optimizing = true;
        registerAnalysis(analyzer);
    }

    /**
     * Runs the solver over a given control flow graph. Prior to calling this,
     * an analysis must be registered using registerAnalysis
     * 
     * @param cfg
     *            The control flow graph to analyze.
     */
    public void visitCFG(ControlFlowGraph cfg) {

        // this needs to come first.
        analysis.preprocess(cfg);
        boolean changed;
        if (analysis.isForward()) {
            HashSet<Quad> exitPredecessors = new HashSet<Quad>();
            QuadIterator it = new QuadIterator(cfg);
            while (it.hasNext()) {
                Quad quad = it.next();
                java.util.Iterator<Quad> succIter = it.successors();
                while (succIter.hasNext()) {
                    Quad succQuad = succIter.next();
                    if (succQuad == null) { // successor is exit
                        exitPredecessors.add(quad);
                    }
                }
            }
            do {
                changed = false;
                QuadIterator quadIter = new QuadIterator(cfg,
                        analysis.isForward());

                while (quadIter.hasNext()) {
                    Quad quad = quadIter.next();

                    java.util.Iterator<Quad> predIter = quadIter.predecessors();
                    Flow.DataflowObject quadIN = analysis.newTempVar();

                    while (predIter.hasNext()) {
                        Quad predecessor = predIter.next();
                        if (predecessor != null) {
                            quadIN.meetWith(analysis.getOut(predecessor));
                        } else {
                            quadIN.meetWith(analysis.getEntry());
                        }
                    }

                    Flow.DataflowObject oldOut = analysis.getOut(quad);

                    analysis.setIn(quad, quadIN);
                    analysis.processQuad(quad);

                    changed = changed
                            || (!oldOut.equals(analysis.getOut(quad)));
                }

                Flow.DataflowObject exitDFO = analysis.newTempVar();
                for (Quad exitPred : exitPredecessors) {
                    exitDFO.meetWith(analysis.getOut(exitPred));
                }
                changed = changed || (!analysis.getExit().equals(exitDFO));
                analysis.setExit(exitDFO);
            } while (changed);
        } else {
            HashSet<Quad> entrySuccessors = new HashSet<Quad>();
            QuadIterator it = new QuadIterator(cfg);
            while (it.hasNext()) {
                Quad quad = it.next();
                java.util.Iterator<Quad> predIter = it.predecessors();
                while (predIter.hasNext()) {
                    Quad predQuad = predIter.next();
                    if (predQuad == null) { // predecessor is entry
                        entrySuccessors.add(quad);
                    }
                }
            }
            do {
                changed = false;
                QuadIterator quadIter = new QuadIterator(cfg,
                        analysis.isForward());

                while (quadIter.hasPrevious()) {
                    Quad quad = quadIter.previous();

                    java.util.Iterator<Quad> succIter = quadIter.successors();
                    Flow.DataflowObject quadOUT = analysis.newTempVar();

                    while (succIter.hasNext()) {
                        Quad successor = succIter.next();
                        if (successor != null) {
                            quadOUT.meetWith(analysis.getIn(successor));
                        } else {
                            quadOUT.meetWith(analysis.getExit());
                        }
                    }

                    Flow.DataflowObject oldIn = analysis.getIn(quad);

                    analysis.setOut(quad, quadOUT);
                    analysis.processQuad(quad);

                    changed = changed || !oldIn.equals(analysis.getIn(quad));
                }
                Flow.DataflowObject entryDFO = analysis.newTempVar();
                for (Quad entrySucc : entrySuccessors) {
                    entryDFO.meetWith(analysis.getIn(entrySucc));
                }
                changed = changed || (!analysis.getEntry().equals(entryDFO));
                analysis.setEntry(entryDFO);

            } while (changed);
        }

        // this needs to come last.
        analysis.postprocess(cfg);
        if(optimizing) {
            while(((OptimizingAnalysis)analysis).doOptimize(cfg));
        }
    }
}
